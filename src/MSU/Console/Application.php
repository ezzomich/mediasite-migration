<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Console;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputInterface;
use MSU\Console\Command\MigrateCommand;

/**
 * Application wrapper to provide for a single command.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class Application extends BaseApplication
{
    /**
     * @var \MSU\Console\Command\MigrateCommand
     */
    protected $command;

    public function __construct($name, $version, MigrateCommand $command)
    {
        // Set the command
        $this->command = $command;

        parent::__construct($name, $version);
    }

    /**
     * Gets the name of the command based on input.
     *
     * @param InputInterface $input The input interface
     *
     * @return string The command name
     */
    protected function getCommandName(InputInterface $input): string
    {
        // This should return the name of your command.
        return 'migrate';
    }

    /**
     * Gets the default commands that should always be available.
     *
     * @return array An array of default Command instances
     */
    protected function getDefaultCommands()
    {
        // Keep the core default commands to have the HelpCommand
        // which is used when using the --help option
        $defaultCommands = parent::getDefaultCommands();

        $defaultCommands[] = $this->command;

        return $defaultCommands;
    }

    /**
     * Overridden so that the application doesn't expect the command
     * name to be the first argument.
     */
    public function getDefinition()
    {
        $inputDefinition = parent::getDefinition();
        // clear out the normal first argument, which is the command name
        $inputDefinition->setArguments();

        return $inputDefinition;
    }
}
