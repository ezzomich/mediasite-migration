<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Console\Command;

use Jenssegers\ImageHash\ImageHash;
use Jenssegers\ImageHash\Implementations\DifferenceHash;
use MSU\Kaltura\Resource as KalturaResource;
use MSU\Mediasite\Presentation;
use MSU\Mediasite\Slide;
use MSU\Mediasite\Stream;
use phpseclib\Net\SFTP;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;
use thiagoalessio\TesseractOCR\TesseractOCR;

/**
 * Command to migrate video and send them to kaltura.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class MigrateCommand extends Command
{
    /** @var \SplFileInfo */
    protected $inbox = null;

    /** @var \SplFileInfo */
    protected $outbox = null;

    /** @var \phpseclib\Net\SFTP */
    protected $sftp = null;

    /** @var KalturaResource */
    protected $kaltura = null;

    /** @var bool */
    protected $uploadFiles = false;

    /** @var bool */
    protected $deleteFiles = false;

    /** @var mixed */
    protected $mapping = null;

    /** @var \Jenssegers\ImageHash\Hash */
    protected $activeHash = null;

    /**
     * Constructor.
     *
     * @param string $inbox   Path to inbox directory
     * @param string $outbox  Path to outbox directory
     * @param SFTP  $sftp     An SFTP object for uploading to Kaltura
     * @param KalturaResource $kaltura A Kaltura resource for working with the API
     */
    public function __construct($inbox, $outbox, SFTP $sftp, KalturaResource $kaltura)
    {
        // Initialize stuff
        $this->inbox =  new \SplFileInfo($inbox);
        $this->outbox = new \SplFileInfo($outbox);
        $this->sftp = $sftp;
        $this->kaltura = $kaltura;

        parent::__construct();
    }

    /**
     * MigrateCommand Destructor.
     */
    public function __destruct()
    {
        // Close the inbox
        $this->inbox = null;

        // Close the outbox
        $this->outbox = null;

        // Close the SFTP connection
        $this->sftp->disconnect();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('migrate')
            ->setDescription('Processes Mediasite presentation files and reformat for Kaltura.')
            ->addOption(
                'owner',
                'o',
                InputOption::VALUE_REQUIRED,
                'If set, always override the owner in kaltura with this value.'
            )
            ->addOption(
                'sftp',
                null,
                InputOption::VALUE_NONE,
                'If set, directly upload to the kaltura drop folder.'
            )
            ->addOption(
                'delete',
                'd',
                InputOption::VALUE_NONE,
                "If set, delete files from the inbox after processing.\nMapping folders will not be deleted."
            )
            ->addOption(
                'filter-slides',
                'f',
                InputOption::VALUE_OPTIONAL,
                "If set, filters consecutive slides with a hamming distance less than or equal to the distance provided.\nIf a distance is not provided, a distance of 30 will be used.\nThe value should be between 0 and 1,056.",
                false
            )
            ->addOption(
                'filter-threshold',
                't',
                InputOption::VALUE_REQUIRED,
                'If set, indicates the minumum number of slides that must be present to initiate filtering.',
                false
            )
            ->addOption(
                'mapping-file',
                'm',
                InputOption::VALUE_REQUIRED,
                "The name of the mapping YAML file to use for enriching data.\nMust be relative to inbox.",
                'mapping.yml'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        // Track command start time
        $startTime = microtime(true);

        // Create a new style component
        $io = new SymfonyStyle($input, $output);

        // Write out the Program title and server time
        $io->title('Mediasite Migration Tool');
        $io->note(sprintf('Current Time: %s', date('D M j G:i:s T Y')));

        // Ensure Inbox is Readable
        if (!$this->inbox->isReadable()) {
            $io->error('Inbox is not readable!');
            return 1;
        }

        // Ensure Outbox is writable
        if (!$this->outbox->isWritable()) {
            $io->error('Outbox is not writable!');
            return 1;
        }

        // Process the options
        $owner = $input->getOption('owner');
        if ($owner !== null) {
            // Log the owner
            $io->note(sprintf('Owner shall be : %s.', $owner));
        }

        $this->uploadFiles = $input->getOption('sftp');
        if ($this->uploadFiles === true) {
            $io->note('Using SFTP mode.');
        }

        $this->deleteFiles = $input->getOption('delete');
        if ($this->deleteFiles === true) {
            $io->note('Using delete mode.');
        }

        $filterSlides = $input->getOption('filter-slides');
        $filterSlides = ($filterSlides === null) ? 30 : $filterSlides;
        if ($filterSlides !== false) {
            $io->note(sprintf('Filtering slides with hamming distance less than or equal to %d.', $filterSlides));
        }

        $filterThreshold = $input->getOption('filter-threshold');
        if ($filterThreshold !== false) {
            $io->note(sprintf('Slide filtering threshold is %d.', $filterThreshold));
        }

        // Determine the path to the YAML file
        $mappingFile = $input->getOption('mapping-file');
        if ($mappingFile !== 'mapping.yml') {
            $io->note(sprintf('Alternate mapping file provided: %s.', $mappingFile));
        }
        $mappingFile = $this->inbox->getRealpath() . DIRECTORY_SEPARATOR . $mappingFile;

        // Test if we can read the file
        if (!@is_readable($mappingFile)) {
            $io->caution('Missing Mapping file.');
            $this->mapping = [];
        } else {
            try {
                // Try to parse the mapping config file
                $yaml = new Parser();
                $this->mapping = $yaml->parse(file_get_contents($mappingFile));
            } catch (ParseException $e) {
                $io->error(
                    sprintf(
                        'Unable to parse the YAML string: %s.',
                        $e->getMessage()
                    )
                );
                return 1;
            }
        }

        // Get the Zip and XML files in the Folder
        $finder = new Finder();
        $files = $finder->depth('< 3')->files()->name('*.zip')->name('*.xml')->in($this->inbox->getRealPath());

        // Print the number of files found
        $fileCount = iterator_count($files);
        $io->text(sprintf('%d files found.', $fileCount));

        // Foreach file extract the goodness
        $i = 1;
        foreach ($files as $file) {
            // Open the package and read it's manifest
            try {
                // Convert the file to a presenation
                $package = new Presentation($file);
            } catch (\UnderflowException $e) {
                // Skip this package if reading the manifest fails
                $io->error(sprintf('Failed to process Archive: %s', $e->getMessage()));
                // Destroy the package and continue
                unset($package);
                continue;
            }

            // Print a message
            $io->section(
                sprintf(
                    'Processing Presentation %d / %d - "%s"',
                    $i++,
                    $fileCount,
                    $package->getPackageName()
                )
            );

            // Get the name of the parent folder of the presentation
            // We'll use this for mappings
            $parentDir = $package->getPackageParentDirectoryName();
            $io->text(sprintf('Parent directory: %s', $parentDir));

            // Generate a unique ID for later
            $rootId = $package->getId() . '-' . time();

            // Calculate the Owner
            // Use the one from the command line
            // or if not set, use the one from the mapping file
            // or if not set, use the one from the package
            $owner = ($owner !== null) ? $owner : ((isset($this->mapping[$parentDir]['owner'])) ? trim($this->mapping[$parentDir]['owner']) : $package->getOwner());

            // Start writing the new XML
            $newXml = new \SimpleXMLElement('<?xml version="1.0"?><mrss xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="1.0"></mrss>');

            $channel = $newXml->addChild('channel');
            $item = $channel->addChild('item');
            $item->addChild('action', "add");
            $item->addChild('type', "1");
            $item->addChild('referenceId', $rootId);
            $item->addChild('userId', $owner);

            // Pull co-editors and co-publishers from the mapping file
            $entitledUsersEdit = $item->addChild('entitledUsersEdit');
            if (
                isset($this->mapping[$parentDir]['coeditors'])
                && is_array($this->mapping[$parentDir]['coeditors'])
            ) {
                foreach ($this->mapping[$parentDir]['coeditors'] as $user) {
                    $entitledUsersEdit->addChild('user', trim(strtolower($user)));
                }
            }
            $entitledUsersPublish = $item->addChild('entitledUsersPublish');
            if (
                isset($this->mapping[$parentDir]['copublishers'])
                && is_array($this->mapping[$parentDir]['copublishers'])
            ) {
                foreach ($this->mapping[$parentDir]['copublishers'] as $user) {
                    $entitledUsersPublish->addChild('user', trim(strtolower($user)));
                }
            }

            // Add name and description
            $item->addChild('name', htmlspecialchars($package->getName(), ENT_XML1));
            $item->addChild('description', htmlspecialchars($package->getDescription(), ENT_XML1));

            // Add the folders as tags
            $tags = $item->addChild('tags');
            foreach ($package->getTags() as $tag) {
                $tags->addChild('tag', htmlspecialchars($tag, ENT_XML1));
            }

            // We'll get the categories from the mapping file
            $categories = $item->addChild('categories');
            // Add everything to redbox
            if ($this->kaltura->getDefaultCategoryId() !== -1) {
                $categories->addChild('categoryId', $this->kaltura->getDefaultCategoryId());
            }

            if (
                isset($this->mapping[$parentDir]['categories'])
                && is_array($this->mapping[$parentDir]['categories'])
            ) {
                foreach ($this->mapping[$parentDir]['categories'] as $category) {
                    $categories->addChild('categoryId', trim($category));
                }
            }

            $item->addChild('conversionProfileId', $this->kaltura->getConversionProfileId());
            $item->addChild('media')->addChild('mediaType', '1');

            // We need to get a video stream, prefer video 2, but fall back to Video 1 if 2 doesn't exist, and then 3
            // This holds the priority order for processing
            $streamPriority = [Stream::VIDEO1, Stream::VIDEO2, Stream::VIDEO3];
            // This will hold a thumbnail for the Slides if the first is missing
            $streamThumbnail = null;
            // First, lets find the primary stream -- first available
            foreach ($streamPriority as $key => $streamId) {
                // Pop out of the list once touched. 
                unset($streamPriority[$key]);

                // The first one we've found is primary, so process and stop
                if (($stream = $package->getVideoStream($streamId)) !== null) {
                    $io->text('Processing Primary Video Feed...');

                    // Start with the primary stream--this probably not what i want, but it's something
                    // Typically for us, stream 1 is the slides--this is probably stream 2, but could be 1
                    $streamThumbnail = $stream->getThumbnail();

                    if (($primaryVideo = $package->extractContentFile($stream->getVideo(), $this->outbox, $rootId)) !== null) {
                        // Add the content file
                        $item->addChild('contentAssets')
                            ->addChild('content')
                            ->addChild('dropFolderFileContentResource')
                            ->addAttribute('filePath', $primaryVideo->getFilename());
                        $this->migrateFile($primaryVideo, $io);
                    }

                    break;
                }
            }

            // Now we can process the remaining streams
            foreach ($streamPriority as $streamId) {
                if (($stream = $package->getVideoStream($streamId)) !== null) {
                    $io->text('Processing Secondary Videos...');
                    // Video 1 or Video 3 (this is for asthetics and odd choice though)
                    $newItem = $channel->addChild('item');
                    $newItem->addChild('action', "add");
                    $newItem->addChild('type', "1");
                    $newItem->addChild('parentReferenceId', $rootId);
                    $newItem->addChild('userId', $owner);
                    $newItem->addChild('name', htmlspecialchars($package->getName(), ENT_XML1));
                    $newItem->addChild('conversionProfileId', $this->kaltura->getConversionProfileId());
                    $newItem->addChild('media')->addChild('mediaType', '1');

                    // If we're here, take the thumb from stream 1
                    //if ($streamId == Stream::VIDEO1) {
                    //    $streamThumbnail = $stream->getThumbnail();
                    //}

                    if (($alternateVideo = $package->extractContentFile($stream->getVideo(), $this->outbox, $rootId)) !== null) {
                        $newItem->addChild('contentAssets')
                            ->addChild('content')
                            ->addChild('dropFolderFileContentResource')
                            ->addAttribute('filePath', $alternateVideo->getFilename());
                        $this->migrateFile($alternateVideo, $io);
                    }

                    $alternateSyncData = $newItem->addChild('customDataItems')
                    ->addChild('customData');
$alternateSyncData->addAttribute('metadataProfile', 'apinotificationssyncdata');
$alternateSyncData->addChild('xmlData')->addChild('metadata')->addChild('SyncStatus', 'Sync Done');
                }
            }

            // Process slides if they exist
            if (($slides = $package->getSlides()) !== null) {
                // Count the number of slides
                $slideCount = count($slides);
                $io->text(sprintf('Processing %d Slides...', $slideCount));

                // Add the root scenes element
                $scenes = $item->addChild('scenes');

                // Create a hasher for comparing slides
                $hasher = new ImageHash(new DifferenceHash(32));

                // Add a Slide 0 if the First Slide doesn't start at 0MS
                // If we don't have a thumbnail set the time to 0
                if (isset($slides[0]) && $slides[0]->getStartTime() != 0) {
                    if ($streamThumbnail !== null) {
                        array_unshift($slides, new Slide(0, 0, '', $streamThumbnail));
                    } else {
                        $slides[0]->setStartTime(0);
                    }
                }

                $j = 1;
                foreach ($slides as $slide) {
                    // Move the file to the Outbox
                    if (($slideFile = $package->extractContentFile($slide->getFileName(), $this->outbox, $rootId)) !== null) {
                        // If filter slides is ON AND either a threshold isn't set, or the slidecount exceeds the threshold
                        if ($filterSlides !== false && ($filterThreshold === false || $slideCount >= (int)$filterThreshold)) {
                            // Compute a hash
                            $slideHash = $hasher->hash($slideFile->getRealPath());
                            if ($this->activeHash !== null) {
                                // Get the hamming distance between this slide and the current
                                $distance = $hasher->distance($this->activeHash, $slideHash);

                                // Skip this slide if it looks like the last one
                                if ($distance <= (int)$filterSlides) {
                                    $io->writeln(
                                        sprintf('Distance: %d, Skipping: Slide %d', $distance, $slide->getNumber()),
                                        OutputInterface::OUTPUT_NORMAL|OutputInterface::VERBOSITY_DEBUG
                                    );
                                    // remove the skipped slide and continue
                                    unlink($slideFile->getRealPath());
                                    continue;
                                } else {
                                    $io->writeln(
                                        sprintf('Distance: %d', $distance),
                                        OutputInterface::OUTPUT_NORMAL|OutputInterface::VERBOSITY_DEBUG
                                    );
                                }
                            }

                            // Set active hash to the hash of the current slide
                            $this->activeHash = $slideHash;
                        }

                        // Make a scene
                        $scene = $scenes->addChild('scene-thumb-cue-point');
                        $scene->addChild('sceneStartTime', $slide->getFormattedStartTime());
                        $scene->addChild('title', htmlspecialchars(trim('Slide ' . $j++ . ' ' .$slide->getTitle()), ENT_XML1));

                        // Fill the description with OCRd text of the slide
                        $scene->addChild(
                            'description',
                            htmlspecialchars((new TesseractOCR($slideFile->getRealPath()))->run(), ENT_XML1)
                        );

                        $scene->addChild('slide')
                            ->addChild('dropFolderFileContentResource')
                            ->addAttribute('filePath', $slideFile->getFilename());
                        $this->migrateFile($slideFile, $io);
                    }
                }
            }

            // Process captions if they exist
            if (($captions = $package->getCaptionsFile()) !== null) {
                $io->text('Processing Subtitles...');

                $subtitle = $item->addChild('subTitles')->addChild('subTitle');
                $subtitle->addAttribute('isDefault', 'true');
                $subtitle->addAttribute('format', '1');
                $subtitle->addAttribute('lang', 'English (American)');
                $subtitle->addChild('tags');

                if (($captionsFile = $package->extractContentFile($captions, $this->outbox, $rootId)) !== null) {
                    $subtitle->addChild('dropFolderFileContentResource')
                        ->addAttribute('filePath', $captionsFile->getFilename());
                    $this->migrateFile($captionsFile, $io);
                }
            }

            // Add custom data field
            $customDataItems = $item->addChild('customDataItems');
            $customSyncData = $customDataItems->addChild('customData');
            $customSyncData->addAttribute('metadataProfile', 'apinotificationssyncdata');
            $customSyncData->addChild('xmlData')->addChild('metadata')->addChild('SyncStatus', 'Sync Done');

            $creationDate = $package->getRecordingDateTime();
            if ($creationDate !== null) {
              $customDateData = $customDataItems->addChild('customData');
              $customDateData->addAttribute('metadataProfile', 'migration');
              $customDateData->addChild('xmlData')->addChild('metadata')->addChild('RecordingDateTime', $creationDate->format('U'));
            }

            // Move the XML into position
            $io->text('Generating XML...');
            $newFile = $this->outbox->getRealPath() . DIRECTORY_SEPARATOR . $rootId . '.xml';
            if (file_put_contents($newFile, $newXml->asXML()) !== false) {
                $this->migrateFile(new \SplFileInfo($newFile), $io);
            }

            // Close the package
            $package->close();

            // Delete the file from the inbox if requested
            if ($this->deleteFiles) {
                $io->text(sprintf('Deleting Package: %s', $package->getPackageName()));
                $package->delete();
            }
        }

        $io->success(sprintf(
            'Operations Complete! Execution took %01.2f seconds.',
            (microtime(true) - $startTime)
        ));

        return 0;
    }

    /**
     * Migrates a file from a package to a dropfolder
     *
     * @param \SplFileInfo $source The source file to migrate
     * @param SymfonyStyle $output The output interface to print to
     * @return void
     */
    private function migrateFile($source, SymfonyStyle $io)
    {
        // Do nothing if we're not in upload mode
        if ($this->uploadFiles) {
            // SFTP to kaltura drop folder from the outbox
            $size = $source->getSize();
            $io->text(sprintf("Uploading file: %s", $source->getFilename()));
            $progress = $io->createProgressBar($size);
            $progress->start();

            $this->sftp->put(
                $source->getFilename(),
                $source->getRealPath(),
                SFTP::SOURCE_LOCAL_FILE,
                -1,
                -1,
                function ($sent) use ($progress, $size) {
                    $progress->setProgress($sent);
                    if ($sent == $size) {
                        $progress->finish();
                    }
                }
            );

            $io->newLine(2);
            unlink($source->getRealPath());
        }
    }
}
