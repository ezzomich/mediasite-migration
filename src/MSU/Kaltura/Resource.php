<?php

/*
 * This file is part of the MediaSite Package Processer.
 *
 * (c) Michigan State University
 */

namespace MSU\Kaltura;

use Psr\Log\LoggerInterface;
use Kaltura\Client\Client;
use Kaltura\Client\Configuration;
use Kaltura\Client\Enum\SessionType;
use Kaltura\Client\ApiException;
use Kaltura\Client\ClientException;

/**
 * Manages Kaltura client resources.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class Resource
{
    const DEFAULT_KS_EXPIRY = 14400;

    /**
     * @var Client
     */
    private $userClient = null;

    /**
     * @var Client
     */
    private $userClientNoEntitlement = null;

    /**
     * @var Client
     */
    private $adminClient = null;

    /**
     * @var Client
     */
    private $adminClientNoEntitlement = null;

    /**
     * @var string
     */
    private $partnerId = null;

    /**
     * @var string
     */
    private $userSecret = null;

    /**
     * @var string
     */
    private $adminSecret = null;

    /**
     * @var string
     */
    private $privacyContext = '';

    /**
     * @var int
     */
    private $defaultCategoryId = -1;

    /**
     * @var int
     */
    private $conversionProfileId = -1;

    /**
     * @var string
     */
    private $serviceUrl = 'http://www.kaltura.com/';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var LoggerInterface;
     */
    private $logger = null;

    /**
     * Constructor.
     *
     * @param string $partnerId
     * @param string $secret
     * @param string $adminSecret
     * @param string $privacyContext
     * @param string $serviceUrl
     */
    public function __construct(string $partnerId, string $userSecret, string $adminSecret, string $privacyContext = '', string $serviceUrl = null)
    {
        $this->partnerId = $partnerId;
        $this->userSecret = $userSecret;
        $this->adminSecret = $adminSecret;
        $this->privacyContext = $privacyContext;

        if ($serviceUrl !== null) {
            $this->serviceUrl = $serviceUrl;
        }
    }

    /**
     * Sets the conversion profile Id to use
     *
     * @param int $conversionProfile The new conversion profile Id
     * 
     * @return Resource
     */
    public function setConversionProfileId(int $conversionProfileId): Resource
    {
        // Store the new conversionProfile
        $this->conversionProfileId = $conversionProfileId;

        return $this;
    }

    /**
     * Gets the conversion Profile Id
     *
     * @return int
     */
    public function getConversionProfileId(): int
    {
        return $this->conversionProfileId;
    }

    /**
     * Sets the default category Id to publish to
     *
     * @param int $defaultCategoryId The default category id
     * 
     * @return Resource
     */
    public function setDefaultCategoryId(int $defaultCategoryId): Resource
    {
        // Store the new conversionProfile
        $this->defaultCategoryId = $defaultCategoryId;

        return $this;
    }

    /**
     * Gets the default Category Id
     *
     * @return int
     */
    public function getDefaultCategoryId(): int
    {
        return $this->defaultCategoryId;
    }

    /**
     * Set userId.
     *
     * @param string $userId The Id of the Kaltura User making using the Client
     *
     * @return Resource
     */
    public function setUserId($userId): Resource
    {
        // Store the new userId
        $this->userId = $userId;

        return $this;
    }

    /**
     * Sets a logger to use with the Service.
     *
     * @param LoggerInterface $logger The logger to use
     *
     * @return Resource
     */
    public function setLogger(LoggerInterface $logger): Resource
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Get a Kaltura user client.
     *
     * @return Client
     */
    public function getUserClient(): Client
    {
        if ($this->userClient === null) {
            $this->userClient = $this->initClient(SessionType::USER);
        }

        return $this->userClient;
    }

    /**
     * Get a Kaltura user client with no entitlements.
     *
     * @return Client
     */
    public function getUserClientNoEntitlement(): Client
    {
        if ($this->userClientNoEntitlement === null) {
            $this->userClientNoEntitlement = $this->initClient(SessionType::USER, false);
        }

        return $this->userClientNoEntitlement;
    }

    /**
     * Get a Kaltura admin client.
     *
     * @return Client
     */
    public function getAdminClient(): Client
    {
        if ($this->adminClient === null) {
            $this->adminClient = $this->initClient(SessionType::ADMIN);
        }

        return $this->adminClient;
    }

    /**
     * Get a Kaltura admin client with no entitlements.
     *
     * @return Client
     */
    public function getAdminClientNoEntitlement(): Client
    {
        if ($this->adminClientNoEntitlement === null) {
            $this->adminClientNoEntitlement = $this->initClient(SessionType::ADMIN, false);
        }

        return $this->adminClientNoEntitlement;
    }

    /**
     * Initialize a kaltura client with a KS.
     *
     * @param KalturaSessionType $clientType
     * @param bool               $enableEntitlement Create the ks with entitlement
     *
     * @return KalturaClient
     *
     * @throws Exception If unable to connect to Kaltura
     */
    private function initClient($clientType = SessionType::USER, bool $enableEntitlement = true): Client
    {
        // Configuration
        $config = new Configuration();
        $config->setServiceUrl($this->serviceUrl);
        $config->setCurlTimeout(120);

        // Create a new client
        $client = new Client($config);

        // Determine the privileges to use for this client
        $privileges = $this->getDefaultPrivileges($enableEntitlement);

        // Set the KS on the client
        $client->setKs($client->generateSessionV2(
            $this->adminSecret,
            $this->userId,
            $clientType,
            $this->partnerId,
            self::DEFAULT_KS_EXPIRY,
            $privileges
        ));

        if ($this->logger !== null) {
            $this->logger->debug(
                'KalturaResource: Initialized {type} client for {user} with {entitlements}.',
                array('type' => $clientType, 'user' => $this->userId, 'entitlements' => $privileges)
            );
        }

        // Check connectivity
        try {
            $client->getSystemService()->ping();
        } catch (ApiException $ex) {
            die('Ping failed with api error: '.$ex->getMessage());
        } catch (ClientException $ex) {
            die('Ping failed with client error: '.$ex->getMessage());
        }

        return $client;
    }

    /**
     * Clear existing clients.
     *
     * @return Resource
     */
    public function reInitClients(): Resource
    {
        // Reset the clients
        $this->userClient = null;
        $this->userClientNoEntitlement = null;
        $this->adminClient = null;
        $this->adminClientNoEntitlement = null;

        return $this;
    }

    /**
     * Get privileges for KS.
     *
     * @param bool   $enableEntitlement Create the ks with entitlement
     * @param string $userId
     *
     * @return string
     */
    private function getDefaultPrivileges(bool $enableEntitlement, string $userId = ''): string
    {
        $privileges = '';

        if ($enableEntitlement && !empty($this->privacyContext)) {
            $privileges = "privacycontext:{$this->privacyContext},enableentitlement";
        } else {
            $privileges = 'disableentitlement';
        }

        // For making sure the API is aware of specific user in case it needs to use dedicated resource for that user
        if (!empty($this->userId)) {
            $privileges .= ",sessionkey:{$this->userId}";
        }

        return $privileges;
    }
}
