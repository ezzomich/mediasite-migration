<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite\Package;

/**
 * Package Interface for Package formats
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 * @copyright Michigan State University
 */
interface PackageInterface
{
    /**
     * Opens the Package
     *
     * @param \SplFileInfo $package The package to open
     * @return bool
     */
    public function open(\SplFileInfo $package): bool;

    /**
     * Reads the manifest of a package
     *
     * @return \SimpleXMLElement|null The manifest or null
     */
    public function readManifest(): ?\SimpleXMLElement;

    /**
     * Closes the Package
     *
     * @return void
     */
    public function close(): void;

    /**
     * Deletes the Package
     *
     * @return bool
     */
    public function delete(): bool;

    /**
     * Extracts a content file from the package to destfolder.
     * If a prefix is provided, it will be appended to the outfile
     *
     * @param string $fileName      The name of the file to extract
     * @param \SplFileInfo $dest    The folder to extract the file to
     * @param string $prefix        The prefix to apply to the name of the out file
     * @return \SplFileInfo|null    The outfile or null if the extract failed
     */
    public function extract(string $fileName, \SplFileInfo $destFolder, string $prefix = ''): ?\SplFileInfo;

    /**
     * Gets the name of the package
     *
     * @return string
     */
    public function name(): string;

    /**
     * Gets the name of the parent directory of the package
     *
     * @return string
     */
    public function getParentDirectoryName(): string;
}
