<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite\Package;

/**
 * Package factory class is responsible for instaniating the correct package format
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 * @copyright Michigan State University
 */
class PackageFactory
{
    /**
     * Creates a Package based on filetype
     *
     * @param string $type The type of package to create
     * @throws \UnderflowException if the file type is invalid
     * @return PackageInterface
     */
    public static function create($type): PackageInterface
    {
        // Figure out the right classname
        $package = 'MSU\Mediasite\Package\\' . ucwords(strtolower($type)) . 'Package';

        if (class_exists($package)) {
            return new $package();
        } else {
            throw new \UnderflowException('Invalid Package Type Found.');
        }
    }
}
