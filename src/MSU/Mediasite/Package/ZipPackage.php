<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite\Package;

/**
 * Class for Folder Type Package
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 * @copyright Michigan State University
 */
class ZipPackage implements PackageInterface
{
    /** @var \SplFileInfo */
    private $package = null;

    /** @var \ZipArchive */
    private $zip = null;

    /**
     * Opens a Zip file
     *
     * @param \SplFileInfo $file The Zip file to open
     * @throws \UnderflowException If the Zip cannot be opened
     * @return boolean
     */
    function open(\SplFileInfo $file): bool
    {
        // Record package
        $this->package = $file;

        // Check to make sure the package is a file
        if (!$this->package->isReadable()) {
            throw new \UnderflowException('Package is not Readable!');
        }

        // Open the Zip File
        $this->zip = new \ZipArchive();
        $res = $this->zip->open(
            $this->package->getRealPath(),
            \ZipArchive::CHECKCONS
        );

        // Make sure we could open the Zip File
        if ($res !== true) {
            throw new \UnderflowException(sprintf('Code: %s', $res));
        }

        return true;
    }

    /**
     * Package Destructor.
     */
    public function __destruct()
    {
        // Close the Zip file if it hasn't been
        $this->close();

        // Close the Package
        $this->package = null;
    }

    /**
     * Reads the manifest from the package
     *
     * @throws \UnderflowException If the zip canot be opened.
     */
    public function readManifest(): ?\SimpleXMLElement
    {
        // Get the contents of the manifest either version 7 or version 6
        // If the contents are good, store the manifest
        if (
            ($fileContents = $this->zip->getFromName('MediasitePresentation_70.xml')) !== false
            || ($fileContents = $this->zip->getFromName('MediasitePresentation_60.xml')) !== false
        ) {
            $xmlManifest = simplexml_load_string($fileContents);
            if ($xmlManifest !== false) {
                return $xmlManifest;
            }
        }

        return null;
    }

    /**
     * Closes the Presentation
     *
     * @return void
     */
    public function close(): void
    {
        if ($this->zip !== null) {
            $this->zip->close();
            $this->zip = null;
        }
    }

    /**
     * Deletes the Package
     *
     * @return bool
     */
    public function delete(): bool
    {
        // Close the Zip and delete it
        $this->close();
        return unlink($this->package->getRealPath());
    }

    /**
     * Extracts a content file from the presentation to destfolder.
     * If a prefix is provided, it will be appended to the outfile
     *
     * @param string $fileName      The name of the file to extract
     * @param \SplFileInfo $dest    The folder to extract the file to
     * @param string $prefix        The prefix to apply to the name of the out file
     * @return \SplFileInfo|null    The outfile or null if the extract failed
     */
    public function extract(string $fileName, \SplFileInfo $destFolder, string $prefix = ''): ?\SplFileInfo
    {
        $destFile = $destFolder->getRealPath() . DIRECTORY_SEPARATOR . implode('_', array($prefix, $fileName));

        $result = copy(
            'zip://' . $this->package->getRealPath() . '#Content/' . $fileName,
            $destFile
        );

        return ($result) ? new \SplFileInfo($destFile) : null;
    }

    /**
     * Gets the name of the package
     *
     * @return string
     */
    public function name(): string
    {
        return $this->package->getFilename();
    }

    /**
     * Gets the name of the parent directory of the package
     *
     * @return string
     */
    public function getParentDirectoryName(): string
    {
        $filePath = explode(DIRECTORY_SEPARATOR, $this->package->getPath());
        return end($filePath);
    }
}
