<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite\Package;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Class for Folder Type Package
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 * @copyright Michigan State University
 */
class XmlPackage implements PackageInterface
{
    /** @var Filesystem */
    private $filesystem;

    /** @var \SplFileInfo */
    private $folder = null;

    /**
     * XmlPackage Constructor
     */
    function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /**
     * Package Destructor.
     */
    public function __destruct()
    {
        // Close the folder
        $this->folder = null;
    }
    /**
     * Open package
     *
     * @param \SplFileInfo $file The answer text of a question
     */
    public function open(\SplFileInfo $file): bool
    {
        // Set the folder to the parent of the file
        $this->folder = new \SplFileInfo($file->getPath());

        if (!$this->folder->isDir()) {
            throw new \UnderflowException('Package is not a folder!');
        }

        return true;
    }

    /**
     * Closes the Presentation
     *
     * @return void
     */
    public function close(): void
    {
        // Nothing to do here
    }

    /**
     * Deletes the Package
     *
     * @return bool
     */
    public function delete(): bool
    {
        // Delete the file
        try {
            $this->filesystem->remove($this->folder->getRealPath());
        } catch (IOExceptionInterface $exception) {
            return false;
        }

        return true;
    }

    /**
     * Reads the manifest from the package
     *
     * @throws \UnderflowException If the zip canot be opened.
     */
    public function readManifest(): ?\SimpleXMLElement
    {
        // Get the contents of the manifest either version 7 or version 6
        // If the contents are good, store the manifest
        if (
            ($fileContents = @file_get_contents($this->folder->getRealPath() . DIRECTORY_SEPARATOR . 'MediasitePresentation_70.xml')) !== false
            || ($fileContents = @file_get_contents($this->folder->getRealPath() . DIRECTORY_SEPARATOR . 'MediasitePresentation_60.xml')) !== false
        ) {
            $xmlManifest = simplexml_load_string($fileContents);
            if ($xmlManifest !== false) {
                return $xmlManifest;
            }
        }

        return null;
    }
 
    /**
     * Extracts a content file from the presentation to destfolder.
     * If a prefix is provided, it will be appended to the outfile
     *
     * @param string $fileName      The name of the file to extract
     * @param \SplFileInfo $dest    The folder to extract the file to
     * @param string $prefix        The prefix to apply to the name of the out file
     * @return \SplFileInfo|null    The outfile or null if the extract failed
     */
    public function extract(string $fileName, \SplFileInfo $destFolder, string $prefix = ''): ?\SplFileInfo
    {
        $destFile = $destFolder->getRealPath() . DIRECTORY_SEPARATOR . implode('_', array($prefix, $fileName));

        try {
            $this->filesystem->copy(
                implode(DIRECTORY_SEPARATOR, array($this->folder->getRealPath(), 'Content', $fileName)),
                $destFile
            );
        } catch (IOExceptionInterface $exception) {
            return null;
        }

        return new \SplFileInfo($destFile);
    }

    /**
     * Gets the name of the package
     *
     * @return string
     */
    public function name(): string
    {
        return $this->folder->getBasename();
    }

    /**
     * Gets the name of the parent directory of the package
     *
     * @return string
     */
    public function getParentDirectoryName(): string
    {
        $filePath = explode(DIRECTORY_SEPARATOR, $this->folder->getPath());
        return end($filePath);
    }
}
