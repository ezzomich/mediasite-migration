<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite;

/**
 * Wraps MediaSite Slides.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class Slide
{
    /** @var int */
    private $number;

    /** @var int */
    private $time;

    /** @var string */
    private $title;

    /** @var string */
    private $fileName;

    /**
     * Slide Constructor.
     *
     * @param int $number            The slide number
     * @param int $time              The start time of the slide in milliseconds
     * @param string $title          The title of the slide
     * @param string $fileName       The filename for the slide
     */
    public function __construct(int $number, int $time, string $title, string $fileName)
    {
        $this->number = $number;
        $this->time = $time;
        $this->title = $title;
        $this->fileName = $fileName;
    }

    /**
     * Sets the number of the slide
     *
     * @param int $number The number for the slide
     * 
     * @return Slide
     */
    public function setNumber(int $number): Slide
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Gets the number of the slide
     *
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * Sets the title of the slide
     *
     * @param string $title The title for the slide
     * 
     * @return Slide
     */
    public function setTitle(string $title): Slide
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the title of the slide
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the filename of the slide
     *
     * @param string $fileName The filename for the slide
     * 
     * @return Slide
     */
    public function setFileName(string $fileName): Slide
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Gets the filename of the slide
     *
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * Set time  in milliseconds
     *
     * @param int $time The start time for the slide in milliseconds
     * 
     * @return Slide
     */
    public function setStartTime(int $time): Slide
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Gets time in milliseconds
     *
     * @return int
     */
    public function getStartTime(): int
    {
        return $this->time;
    }

    /**
     * Gets time Formatted as hh:mm:ss.sss
     *
     * @return string
     */
    public function getFormattedStartTime(): string
    {
        $whole = $this->time;
        $hours = floor($whole / (60 * 60 * 1000));
        $whole = $whole - ($hours * (60 * 60 * 1000));
        $minutes = floor($whole / (60 * 1000));
        $whole = $whole - ($minutes * (60 * 1000));
        $seconds = floor($whole / 1000);
        $whole = $whole - ($seconds * 1000);

        return sprintf('%02u:%02u:%02u.%03u', $hours, $minutes, $seconds, $whole);
    }

    /**
     * Gets the slide filename
     *
     * @param string $fileName  A filename with a token for a slidenumber
     * @param int $slideNumber  The slide number.
     * @return string           The filename of the slide with the token replaced according to the rules of the token
     */
    public static function getSlideFileName(string $fileName, int $slideNumber): string
    {
        return preg_replace_callback(
            '/\{(\w+)(?:\}|:(\D)(\d+)\})/m',
            function ($matches) use ($slideNumber) {
                return sprintf('%' . strtolower($matches[1] . $matches[3] . $matches[2]), $slideNumber);
            },
            $fileName
        );
    }
}
