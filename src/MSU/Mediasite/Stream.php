<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite;

/**
 * Wraps MediaSite Stream.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class Stream
{
    const VIDEO1 = 0;
    const SLIDES = 2;
    const PRESENTATION = 3;
    const VIDEO2 = 4;
    const VIDEO3 = 5;

    /** @var int */
    private $type;

    /** @var int */
    private $priority;

    /** @var string */
    private $thumbnail;

    /** @var string */
    private $video;

    /**
     * Stream Constructor.
     *
     * @param int $type            The stream type
     * @param int $priority        The priority of the stream
     * @param string $video        The filename of the video for the stream
     * @param string $thumbnail    The filename of the thumbnail of the stream
     */
    public function __construct(int $type, int $priority, string $video, string $thumbnail = '')
    {
        $this->type = $type;
        $this->priority = $priority;
        $this->video = $video;
        $this->thumbnail = $thumbnail;
    }

    /**
     * Gets the type of the stream
     *
     * @return int  The stream type
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * Gets the priority of the stream
     *
     * @return int The stream priority
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * Gets the thumbnail for the stream
     *
     * @return string|null The name of the thumbnail file for the stream, or null
     */
    public function getThumbnail(): ?string
    {
        return ($this->thumbnail != '') ? $this->thumbnail : null;
    }

    /**
     * Gets the filename for the stream
     *
     * @return string The name of the video file for the stream
     */
    public function getVideo(): string
    {
        return $this->video;
    }
}
