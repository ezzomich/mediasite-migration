<?php

/*
 * This file is part of the MediaSite Package Processor.
 *
 * (c) Michigan State University
 */

namespace MSU\Mediasite;

use MSU\Mediasite\Package\PackageFactory;
use MSU\Mediasite\Slide;
use MSU\Mediasite\Stream;

/**
 * Wraps MediaSite presentations.
 *
 * @author Michael Ezzo, JD <ezzomich@msu.edu>
 */
class Presentation
{
    /** @var PackageInterface */
    private $package = null;

    /** @var \SimpleXMLElement */
    private $xmlManifest = null;

    /** @var float */
    private $manifestVersion = 7.0;

    /** @var array */
    private $streams = null;

    /**
     * Presentation Constructor.
     *
     * @param \SplFileInfo $file
     *
     * @throws \UnderflowException If the package file cannot be read
     */
    public function __construct(\SplFileInfo $file)
    {
        // Record package and open the file
        $this->package = PackageFactory::create($file->getExtension());
        $this->package->open($file);

        $this->xmlManifest = $this->package->readManifest();
        if (!$this->xmlManifest) {
            $this->xmlManifest = null;
            throw new \UnderflowException('XML manifest is unreadable!');
        }

        // Pull out the version
        if (!empty($this->xmlManifest->Version)) {
            $this->manifestVersion = (float)$this->xmlManifest->Version;
        }
    }

    /**
     * Presentation Destructor.
     */
    public function __destruct()
    {
        // Close the Package
        $this->close();
        $this->package = null;
    }

    /**
     * Closes the Presentation
     *
     * @return void
     */
    public function close(): void
    {
        if ($this->package !== null) {
            $this->package->close();
        }
    }

    /**
     * Extracts a content file from the presentation to destfolder.
     * If a prefix is provided, it will be appended to the outfile
     *
     * @param string $fileName      The name of the file to extract
     * @param \SplFileInfo $dest    The folder to extract the file to
     * @param string $prefix        The prefix to apply to the name of the out file
     * @return \SplFileInfo|null    The outfile or null if the extract failed
     */
    public function extractContentFile(string $fileName, \SplFileInfo $destFolder, string $prefix = ''): ?\SplFileInfo
    {
        return $this->package->extract($fileName, $destFolder, $prefix);
    }

    /**
     * Gets the name of the package
     *
     * @return string
     */
    public function getPackageName(): string
    {
        return $this->package->name();
    }

    /**
     * Gets the Name of the Parent Directory of the Package
     *
     * @return string
     */
    public function getPackageParentDirectoryName(): string
    {
        return $this->package->getParentDirectoryName();
    }

    /**
     * Deletes a package
     *
     * @return bool
     */
    public function delete(): bool
    {
        if ($this->package !== null && $this->package->delete()) {
            $this->package = null;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the XML manifest from the Mediasite presentation
     * 
     * @return \SimpleXMLElement
     */
    public function getManifest(): ?\SimpleXMLElement
    {
        return $this->xmlManifest;
    }

    /**
     * Get the ID for the presentation
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return string|null The presentation id or null if not set
     */
    public function getId(): ?string
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        if (!empty($this->xmlManifest->Properties->Presentation->RootId->Value)) {
            return trim((string)$this->xmlManifest->Properties->Presentation->RootId->Value);
        }

        return null;
    }

    /**
     * Get the owner ID from the presentation
     * If owner is an email, the username will be extracted
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return string|null The presentation owner or null if not set
     */
    public function getOwner(): ?string
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        if (!empty($this->xmlManifest->Properties->Presentation->Owner)) {
            return self::getUserName(trim((string)$this->xmlManifest->Properties->Presentation->Owner));
        }

        return null;
    }

    /**
     * Get the name of the presentation
     * The creation date will be appended to the end
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return string The presentation name
     */
    public function getName(): string
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        $name = '';
        if (!empty($this->xmlManifest->Properties->Presentation->Title)) {
            $name = trim((string)$this->xmlManifest->Properties->Presentation->Title);
        }

        // $createDate = $this->getRecordingDateTime();
        // if ($createDate !== null) {
        //     $name .= ' - ' . $createDate->format('n/j/Y');
        // }

        return $name;
    }

    /**
     * Get the description from the presentation
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return string|null The presentation description or null if not set
     */
    public function getDescription(): ?string
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        if (!empty($this->xmlManifest->Properties->Presentation->Description)) {
            return trim((string)$this->xmlManifest->Properties->Presentation->Description);
        }

        return null;
    }

    /**
     * Get the recording date of the presentation
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return int The presentation name
     */
    public function getRecordingDateTime(): \DateTime
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        if (!empty($this->xmlManifest->Properties->Presentation->RecordDateTimeUtc)) {
            return new \DateTime((string)$this->xmlManifest->Properties->Presentation->RecordDateTimeUtc);
        }

        return null;
    }

    /**
     * Gets tags from the presentation
     *
     * @throws \UnderflowException If no XML Manifest has been loaded
     * @return array An array of tags for the presentation
     */
    public function getTags(): array
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        $tags = [];
        if ($this->xmlManifest !== null) {
            // Add folder names as tags
            foreach ($this->xmlManifest->Properties->Folders->Folder as $folder) {
                // This is a hack to split a tag like "OST453 SS19" into two tags
                $parts = preg_split('/\s+(?=(\D{2}\d{2}))/i', trim(strtolower((string)$folder->Name)));
                $tags = array_merge($tags, $parts);
            }

            // Add the presenters as tags
            foreach ($this->xmlManifest->Properties->Presentation->Presenters->PresentationPresenter as $presenter) {
                $tags[] = trim(strtolower((string)$presenter->FirstName . ' ' . (string)$presenter->LastName));
            }
        }

        return array_unique($tags);
    }

    /**
     * Processes the content streams and fills $this->streams
     *
     * @throws \UnderflowException  If no XML Manifest has been loaded
     */
    private function processContentStreams(): void
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        $this->streams = [];

        if ($this->manifestVersion >= 7.0) {
            foreach ($this->xmlManifest->Properties->ContentRevisions->ContentRevision->Streams->ContentStream as $stream) {
                switch ((int)$stream->StreamType) {
                    case Stream::VIDEO1:
                    case Stream::VIDEO2:
                    case Stream::VIDEO3:
                        foreach ($stream->OnDemandContentList->PresentationContent as $content) {
                            if ((string)$content->MimeType == 'video/mp4') {
                                $this->streams[(int)$stream->StreamType] = new Stream(
                                    (int)$stream->StreamType,
                                    (int)$stream->Priority,
                                    (string)$content->FileName,
                                    (string)$stream->ThumbnailContent->FileName
                                );
                                break;
                            }
                        }
                        break;
                    case Stream::SLIDES:
                        $this->streams[Stream::SLIDES] = [];
                        foreach ($stream->SlideContent->Slides->Slide as $slide) {
                            $this->streams[Stream::SLIDES][] = new Slide(
                                (int)$slide->Number,
                                (int)$slide->Time,
                                trim((string)$slide->Title),
                                Slide::getSlideFileName((string)$stream->SlideContent->FileName, (int)$slide->Number)
                            );
                        }
                        break;
                    case Stream::PRESENTATION:
                        break;
                }
            }
        } else {
            // This is an old video, they were stored differently
            // Do the videos first
            foreach ($this->xmlManifest->Properties->OnDemandContentList->PresentationContent as $content) {
                if ((string)$content->MimeType == 'video/mp4') {
                    $this->streams[Stream::VIDEO1] = new Stream(
                        Stream::VIDEO1,
                        (int)$content->PlaybackOrder,
                        (string)$content->FileName
                    );
                    break;
                }
            }

            // Do the slides
            if (!empty($this->xmlManifest->Properties->SlideContent)) {
                $this->streams[Stream::SLIDES] = [];
                foreach ($this->xmlManifest->Properties->SlideContent->Slides->Slide as $slide) {
                    $this->streams[Stream::SLIDES][] = new Slide(
                        (int)$slide->Number,
                        (int)$slide->Time,
                        trim((string)$slide->Title),
                        Slide::getSlideFileName((string)$this->xmlManifest->Properties->SlideContent->FileName, (int)$slide->Number)
                    );
                }
            }
        }
    }

    /**
     * Gets a video stream for the presentation
     *
     * @param int $stream           The stream number to retrieve
     * @throws \UnderflowException  If no XML Manifest has been loaded
     * @return Stream|null    A Stream object for the stream, or null
     */
    public function getVideoStream($stream = Stream::VIDEO1): ?Stream
    {
        if ($this->streams === null) {
            $this->processContentStreams();
        }

        if (array_key_exists($stream, $this->streams)) {
            return $this->streams[$stream];
        }

        return null;
    }

    /**
     * Gets the slide stream for the presentation
     *
     * @throws \UnderflowException  If no XML Manifest has been loaded
     * @return array|null           An array of slides, or null  
     */
    public function getSlides(): ?array
    {
        if ($this->streams === null) {
            $this->processContentStreams();
        }

        if (array_key_exists(Stream::SLIDES, $this->streams)) {
            return $this->streams[Stream::SLIDES];
        }

        return null;
    }

    /**
     * Gets the captions file for the presentation
     *
     * @throws \UnderflowException  If no XML Manifest has been loaded
     * @return string|null          The name for the captions file, or null
     */
    public function getCaptionsFile(): ?string
    {
        if ($this->xmlManifest === null) {
            throw new \UnderflowException('No XML manifest was found!');
        }

        if (
            !empty($this->xmlManifest->Properties->ContentRevisions->ContentRevision->CaptionContent->Status)
            && (string)$this->xmlManifest->Properties->ContentRevisions->ContentRevision->CaptionContent->Status == 'Completed'
            && !empty($this->xmlManifest->Properties->ContentRevisions->ContentRevision->CaptionContent->FileName)
        ) {
            return (string)$this->xmlManifest->Properties->ContentRevisions->ContentRevision->CaptionContent->FileName;
        }

        return null;
    }

    /**
     * Get's username from an email address
     * @param string $email An email address to parse
     * @return string       The username from the email
     */
    public static function getUserName(string $email): string
    {
        $s = explode('@', $email);
        if (count($s) > 1) {
            array_pop($s); // Remove the last element.
        }
        return implode('@', $s);
    }
}
