# MediaSite Migrate Project

Migrates MediaSite Packages to Kaltura

## Deployment

Install dependencies first.  You'll need php 7.1 or higher, tesseract, and composer.  On a Mac with homebrew, you can do:

```bash
brew install tesseract
brew install composer
```

Checkout the code onto the server and then configure parameters:

```bash
git clone git@gitlab.msu.edu:ezzomich/mediasite-migration.git mediasite-migration
cd mediasite-migration
composer install --no-dev --optimize-autoloader
cp config/parameters.yml.dist config/parameters.yml
```

## Usage

### Command Line

To get help with the command do:

```bash
./migrate --help
```

### Options

A mapping file can be placed in the inbox to provide extra data to the utility.  The format is as follows:

```yaml
FOLDER-NAME:
  owner: netid
  coeditors:
    - netid1
    - netid2
  copublishers:
    - netid3
  categories:
    - 1234567
``` 
